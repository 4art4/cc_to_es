#!/bin/python3
#  Convert Cloud Custodian data to Elasticsearch data
#  Copyright, Art Hill, art.hill@artoflinux.org
#  This code is made available under the Mozilla Public License 2.0 (MPL), specifically the variant which is Incompatible with Secondary Licenses. However, again, if you only want to install and run Bugzilla, you don’t need to worry about that; it’s only relevant if you redistribute the code or any changes you make.
#  The right to alter the terms of the MPL is reserved.
#
# Repo: https://gitlab.com/4art4/cc_to_es
#
#  Requires:
#    - Python 3
#    - Dedicated input directory to read Cloud Custodian  data
#    - Dedicated output directory to write Elasticsearch data
#
#  This assumes that the Cloud Custodian data is in the following format:
#    - Each file is a JSON document
#    - Each record in in it own directory

# Next features:
# turn gzip back on... removed it because it was causing issues with logstash
# remove the source dir (if it does not have a sub-dir)
# launch logstash.  This will be a separate process.
# remove files in ES_data that are more than x days old. Im thinking 10 days.  This is to give time to notice data failing to be ingested by logstash.
# remove empty dirs in ES_data.

# dir to read Cloud Custodian data from
cc_data_dir = './CC_data'

# dir to read Elasticsearch data from
es_data_dir = './ES_data'

# Minimum age of a file to process
min_age_minutes = 5

# name of output file
output_file_name = 'CC_data_preped_for_ES.json'

# Import the os module, for the os.walk function
# ref: https://www.pythoncentral.io/how-to-traverse-a-directory-tree-in-python-guide-to-os-walk/
from os import walk as dir_walk

# Import execute shell commands
# ref: https://janakiev.com/blog/python-shell-commands/
from os import system as bash

# Import make directories
# ref: https://www.tutorialspoint.com/How-can-I-create-a-directory-if-it-does-not-exist-using-Python
from os import makedirs as mkdir
from os import chmod as chmod

# Import os.stat to measure file age
# ref: https://stackoverflow.com/questions/6879364/print-file-age-in-seconds-using-python
from os import stat as file_stat

# Import gzip to open compressed files
# ref: https://stackoverflow.com/questions/10566558/python-read-lines-from-compressed-text-files
# ref: https://www.kite.com/python/answers/how-to-read-the-contents-of-a-gzip-file-in-python
# ref: https://stackoverflow.com/questions/49286135/writing-text-to-gzip-file/49286165
from gzip import open as ugz

# Import regex to replace a substing
# ref:  https://stackoverflow.com/questions/1038824/how-do-i-remove-a-substring-from-the-end-of-a-string
from re import sub as regex_sub

# Import time to get current time
from time import time as now

# Import json to read and write JSON files
import json
from wsgiref.simple_server import WSGIRequestHandler

def fun_debug_json(json_data):
    return json.dumps(json_data, indent=4, sort_keys=True)

def fun_check_file_age(file_path, min_age_minutes):
# Check file age
    if (file_stat(file_path).st_mtime < now() - min_age_minutes * 60):
        return True
    else:
        return False

def fun_read_json_gz(file_path):
# open the gz file and return the json
    with ugz(file_path, 'rb') as f:
    # Read the file
        return json.load(f)

def fun_write_json_gz(file_path, json_data):
# write the json into a gz file
    with ugz(file_path, 'wt') as f:
        json.dump(json_data, f)
    chmod(file_path, 0o666)

def fun_write_json(file_path, json_data):
# write the json to a file
    with open(file_path, 'w') as f:
        json.dump(json_data, f)
    chmod(file_path, 0o666)

output_json_bulk = []

for in_dir, subdirList, fileList in dir_walk(cc_data_dir):
    #print('Found directory: %s' % in_dir)
# Clear data cache of input files
    input_metadata = None
    input_resources = None
    output_json = None
## Make mirrored dir in ES_data
# replace the first dir name with the ES dir name
    out_dir = regex_sub(cc_data_dir, es_data_dir, in_dir)
# make the new dir
    try:
        mkdir(out_dir)
    except OSError as e:
#ignore error if the dir already exists
        if e.errno != 17:
            raise
    chmod(out_dir, 0o777)
    if 'metadata.json.gz' in fileList and 'resources.json.gz' in fileList:

# Check that the files are more than x minutes old.  I think 5 is a sane number.  This is to be sure that Cloud Custodian has finished writing the file.
        if fun_check_file_age(in_dir + "/" + 'metadata.json.gz', min_age_minutes):
            if fun_check_file_age(in_dir + "/" + 'resources.json.gz', min_age_minutes):
# dump json to variables
                input_metadata = fun_read_json_gz(in_dir + "/" + 'metadata.json.gz')
                input_resources = fun_read_json_gz(in_dir + "/" + 'resources.json.gz')
                #print("input_metadata: " + str(fun_debug_json(input_metadata)))
                #print("input_resources: " + str(fun_debug_json(input_resources)))
                if 'policy' in input_metadata:
                    if 'filters' in input_metadata['policy']:
                        input_policy_filters = input_metadata['policy'].pop('filters', None)
                    if 'mode' in input_metadata['policy'] and 'events' in input_metadata['policy']['mode']:
                        input_policy_mode_events = input_metadata['policy']['mode'].pop('events', None)                        
                if 'execution' in input_metadata:
                    input_metadata_execution = input_metadata.pop('execution', None)
                if 'metrics' in input_metadata:
                    input_metadata_metrics = input_metadata.pop('metrics', None)
                #print("input_metadata with metric removed: " + str(fun_debug_json(input_metadata)))
                #print("input_metadata_metrics: " + str(fun_debug_json(input_metadata_metrics)))
                output_metrics = {}
                for this_metric in input_metadata_metrics:
                    metric_name = this_metric.pop('MetricName', None)
                    #print("MetricName: " + str(fun_debug_json(metric_name)))
                    #print("this_metric: " + str(fun_debug_json(this_metric)))
                    output_metrics[metric_name] = this_metric
                #print("output_metrics: " + str(fun_debug_json(output_metrics)))
                output_json = input_metadata
                output_json['metrics'] = output_metrics
                output_json['resources'] = input_resources
                #print("output_json: \n\n\n" + str(fun_debug_json(output_json)) + "\n\n\n")
# Export the data into a single file.
# as gz file
                fun_write_json_gz(out_dir + "/" + output_file_name + ".gz", output_json)
# as file
                #fun_write_json(out_dir + "/" + output_file_name, output_json)
# Add the data to the bulk list... alternate method, but abandoned for now.                
#                output_json_bulk.append(output_json)
#                with ugz("./ES_data_faked/bulk.json.gz", 'at') as f:
#                    json.dump(output_json, f)
#                    f.write(f'\n')


#chmod("./ES_data_faked/bulk.json.gz", 0o666)
#print(str(json.dumps(output_json_bulk)))
#print("output_json_bulk: " + str(output_json_bulk))
#fun_write_json_gz("./ES_data_faked/bulk.json.gz", output_json_bulk)
# remove the source dir (if it does not have a sub-dir)

# launch logstash.  This will be a separate process.

# remove files in ES_data that are more than x days old. Im thinking 10 days.  This is to give time to notice data failing to be ingested by logstash.

# remove empty dirs in ES_data.
